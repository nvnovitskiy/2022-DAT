# Инструменты анализа данных

Выполненные лабораторные работы по курсу: "Инструменты анализа данных"
____

# Лабораторные работы

 № Л/р | Название | Статус| Ссылка на блокнот
 ----- |----------|-------|------
 0 | NumPy |  ✅| [Ссылка](https://github.com/nvnovitskiy/data-analysis-tools/blob/main/lab0_numpy_Novitskiy_6132.ipynb)
 1 | Pandas | ✅| [Ссылка](https://github.com/nvnovitskiy/data-analysis-tools/blob/main/lab1_pandas_Novitskiy_6132.ipynb)
 2 | Визуализация данных | ✅| [Ссылка](https://github.com/nvnovitskiy/data-analysis-tools/blob/main/lab2_visual_Novitskiy_6132.ipynb)
 3 | Классификация в sklearn (многоклассовая, бинарная) | ✅| [Ссылка](https://github.com/nvnovitskiy/data-analysis-tools/blob/main/lab3_classification_Novitskiy_6132.ipynb)
 4 | Деревья решений (классификация, подбор гиперпараметров) | ✅| [Ссылка](https://github.com/nvnovitskiy/data-analysis-tools/blob/main/lab4_trees_Novitskiy_6132.ipynb)
 5 | Catboost (классификация, регрессия) | ✅ | [Ссылка](https://github.com/nvnovitskiy/data-analysis-tools/blob/main/lab5_catboost_Novitskiy_6132.ipynb)
 6 | Линейная регрессия в sklearn | ✅| [Ссылка](https://github.com/nvnovitskiy/data-analysis-tools/blob/main/lab6_linear_models_Novitskiy_6132.ipynb)
 7 | Знакомство с Pytorch | :soon:|
 8 | Кластеризация | ✅| [Ссылка](https://github.com/nvnovitskiy/data-analysis-tools/blob/main/lab8_clasterization_6132_Novitskiy.ipynb)
 9 | Понижение размерности признакового пространства | ✅| [Ссылка](https://github.com/nvnovitskiy/data-analysis-tools/blob/main/lab9_dim_reduction_6132_Novitskiy.ipynb)
 10 | Основы tensorflow  | :soon:|
 11 | Классификация текстов (с помощью TfIdf + LogisticRegression)| :soon:|
